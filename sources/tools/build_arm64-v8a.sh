#!/bin/sh

ndkVer=21.3.6528147 
ndkRoot=/home/sergv/apps/Android/Sdk/ndk/$ndkVer

rm -r /tmp/stockfish
mkdir -p /tmp/stockfish

cmake \
-S `pwd` \
-B /tmp/stockfish \
-DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
-DCMAKE_BUILD_TYPE=Release \
-DANDROID_ABI:STRING=arm64-v8a \
-DANDROID_NATIVE_API_LEVEL:STRING=21 \
-DANDROID_NDK:PATH=$ndkRoot \
-DANDROID_STL:STRING=c++_shared \
-DCMAKE_FIND_ROOT_PATH_MODE_INCLUDE:STRING=BOTH \
-DCMAKE_FIND_ROOT_PATH_MODE_LIBRARY:STRING=BOTH \
-DCMAKE_FIND_ROOT_PATH_MODE_PACKAGE:STRING=BOTH \
-DCMAKE_FIND_ROOT_PATH_MODE_PROGRAM:STRING=BOTH \
-DCMAKE_PREFIX_PATH:STRING=$QT \
-DCMAKE_TOOLCHAIN_FILE:PATH=$ndkRoot/build/cmake/android.toolchain.cmake

